# Scripts AWS
[![Linux](https://svgshare.com/i/Zhy.svg)](https://svgshare.com/i/Zhy.svg) [![AWS](https://badgen.net/badge/icon/aws?icon=aws&label)](https://aws.amazon.com/fr)

### Scriptwriting for AWS during DevOps course
-----

* **Script 1**: Deployment of a static webpage (from a public git repository) in an EC2 instance
* **Script 2**: Deployment of an image from Docker hub in an EC2 instance
* **Script 3**: Deployment of an image from Docker hub in an EC2 instance and display of a container's file
* **Script 4**: Attachment of a volume to an EC2 instance and creation of a snapshot of this volume to restore it
